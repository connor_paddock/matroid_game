# matroid group generator for circuit matroid nonlocal game
# representations of this group should give perfect q-strategies,
# and therefore imply q-representability of matroid

from itertools import chain, combinations

def propPowerSet(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1, len(s)))

def mtdGroup(M):

# define some useful variables (need to potentially fix the indexing for freegroup)

    E = M.groundset()
    C = M.circuits()

# find all proper subsets of the set of circuits

    propSubsets = set()

    for c in C:
        cList = []
        for e in c:
            cList.append(e)
        cPowerSet = propPowerSet(cList)
        for i in cPowerSet:
            propSubsets.add(frozenset(i))
        

# define the generators for the free group on the groundset
    eGenNames = ["x%d" % i for i in range(1,len(E)+1)]
    jGenNames = ["j%d" % j for j in range(1,len(propSubsets)+1)]
    
    fGroup = FreeGroup(eGenNames + jGenNames)

    gens = fGroup.gap().GeneratorsOfGroup()

    iden = gens[0]^0

    groundSetToGen = {}
    subsetToGen = {}

    ctr = 0
    for e in E:
        groundSetToGen[e] = gens[ctr]
        ctr += 1

    for s in propSubsets:
        subsetToGen[s] = gens[ctr]
        ctr += 1

    # we need a bijection from the groundset names to group generator names... actually 
    # just a dictionary from groundset names to group generator names would be sufficient i think.


# relations

    commRel = []

    for c in C:
        for e in c:
            for f in c:
                eElem = groundSetToGen[e]
                fElem = groundSetToGen[f]
                commRel.append(eElem*fElem*eElem*fElem)

    for s in propSubsets:
        for e in s:
            sElem = subsetToGen[s]
            eElem = groundSetToGen[e]
            commRel.append(sElem*eElem*sElem*eElem)
    
    ordRel = []

    for e in E:
        eElem = groundSetToGen[e]
        ordRel.append(eElem*eElem)
    
    for s in propSubsets:
        sElem = subsetToGen[s]
        ordRel.append(sElem*sElem)
    
    consRel = []

    for c in C:
        prod = iden
        for e in c:
            eElem = groundSetToGen[e]
            prod = prod*eElem
        consRel.append(prod)
    
    indepRel = []

    for s in propSubsets:
        prod = subsetToGen[s]
        for e in s:
            eElem = groundSetToGen[e]
            prod = prod*eElem
        indepRel.append(prod)


# Remove any trivial relations.
    relations = commRel + ordRel + consRel + indepRel
    
    while fGroup.one() in relations:
        relations.remove(fGroup.one())

# Create the group via quotient by relations.
    return fGroup.quotient(relations)