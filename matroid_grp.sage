# matroid group generator for circuit matroid nonlocal game
# representations of this group should give perfect q-strategies,
# and therefore imply q-representability of matroid

def matroid_grp(M):

# define some useful variables (need to potentially fix the indexing for freegroup)

    E=[e+1 for e in range(M.size())]

    C_1=[list(c) for c in M.circuits()]

    if type(C_1[0][0])==str:
        C=[[ord(e)-96 for e in c] for c in C_1]
    else:
        C=[[int(x)+1 for x in list(c)] for c in C_1]

    # define the generators for the free group on the groundset
    gens = [var("x%d" % i) for i in E]
    free_group = FreeGroup(gens)

# create a list of lists containing all elements in all circuits
    circuit_elmts = [[e for e in c] for c in C]

# create the list of circuit relations
    circuit_relations = [free_group(p) for p in circuit_elmts]

# create the list of commuting relations
    commuting_relations = []
    for r in circuit_elmts:
        for i in r:
            for j in r:
                commuting_relations.append(free_group([i, j, -i, -j]))

# create the list of order relations (below is order 2)
    for i in E:
        commuting_relations.append(free_group([i, i]))

# TODO: include the order relations e.g this depends on the field, maybe it
# should be an input parameter of the function

# Remove any trivial relations.
    relations = commuting_relations + circuit_relations
    while free_group.one() in relations:
        relations.remove(free_group.one())

# Create the group via quotient by relations.
    return free_group.quotient(relations)

K=matroid_grp(M)
