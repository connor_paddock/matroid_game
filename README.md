# README #

this repo contains code for generating a group from a matroid, the idea is that
this group will encode representability as representations of the group.

### What is this repository for? ###

* Exploring perfect quantum strategies for the matroid circuit nonlocal game
over Fp

### How do I get set up? ###

* open a SAGEmath session in the repo and use the command `attach("matroid_grp.sage")`
* this will load the .sage script into the current sage session


* I have also included my code for the incidence group generator for the
incidence nonlocal games I talk about in my thesis. These are the F2 LCS
game for Ax=b where the matrix A is the incidence matrix of a graph and
b:V-->Z_2 is a nonproper 2-colouring of the vertices. This code serves
as a template for what we want in the matroid case.
